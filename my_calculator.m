'task1. value of h/(2pi)'
 h=6.62607015*10^-34; %h from NIST'
 ans1=h/2*pi
 
 'task2. Value of sin(30�/e)'
 ans2=sind(30/exp(1))
 
 'task3. Value of 0x00123d3 / (2.455�10^23)'
 b=hex2dec('00123d3'); %change hexadecimal to decimal
 ans3=b/(2.455*10^23)
 
 'task4. Value of sqrt(e?pi)'
 ans4=sqrt(exp(1)/pi) 
 
 'task5. show tenth number of Pi'
 pizmienna=pi*1e10; %1e10 = 10^10
 ans5=floor(mod(pizmienna,10)) %floor returns integer, mod is devision with reminder
  
  'task6. How many days passed since my birthday'
  firstdate=datenum(1999,08,30); %datenum show how many days passed from January 0, 0000 
  seconddate=datenum(2019,10,18);
  ans6=abs(firstdate-seconddate)
  
  'task7. Value of equation'
  R=6.371e6; %R is radius of Earth
  x2=log(R/1e5); %value of ln(R/10^5)
  x3=hex2dec('aabb'); %change hexadecimal to decimal
  x4=exp((sqrt(7)/2)-x2); %value of numerator
  ans7=atan(x4/x3)
   
  'task8. how many atoms is in 0.2 micromol of ethyl alcohol'
  stalaAvogadro = 6.02214076e23; % Avogadro constant taken from NIST
  ans8=0.2*stalaAvogadro*9*1e-6
   
  'task9. how many of � all atoms is 13C'
  ans9=ans8*2/9*0.01


 
 